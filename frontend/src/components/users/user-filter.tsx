import React, { useState, useCallback } from 'react';
import { FormControl, InputLabel, MenuItem } from '@material-ui/core';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

interface IUserFilter {
  handlerFilter: (params: { type: string; value: string }) => void;
}

const filtersAvailable = ['title', 'gender', 'company'];

export const UserFilter: React.FC<IUserFilter> = ({ handlerFilter }) => {
  const [type, setType] = useState('');
  const [value, setValue] = useState('');

  const handleFilter = useCallback(() => {
    if (value) {
      handlerFilter({ value, type });
    }
  }, [type, value, handlerFilter]);

  return (
    <div style={{ display: 'flex', padding: '20px' }}>
      <TextField
        label='Value'
        variant='outlined'
        onChange={(event) => setValue(event.target.value as string)}
      />
      <FormControl fullWidth>
        <InputLabel id='demo-simple-select-label'>Type</InputLabel>
        <Select
          label='type'
          value={type}
          onChange={(event: SelectChangeEvent) =>
            setType(event.target.value as string)
          }
        >
          {filtersAvailable.map((item: string) => (
            <MenuItem value={item} key={item}>
              {item}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <Button variant='outlined' onClick={handleFilter}>
        filter
      </Button>
    </div>
  );
};
