import {
  JsonController,
  Get,
  HttpCode,
  NotFoundError,
  Param,
  QueryParam,
  BadRequestError,
} from 'routing-controllers';
import { PeopleProcessing } from '../services/people_processing.service';

const peopleProcessing = new PeopleProcessing();

const filtersAvailable = ['title', 'gender', 'company'];

@JsonController('/people', { transformResponse: false })
export default class PeopleController {
  @HttpCode(200)
  @Get('/all')
  getAllPeople() {
    const people = peopleProcessing.getAll();

    if (!people) {
      throw new NotFoundError('No people found');
    }

    return {
      data: people.slice(0, 20),
    };
  }

  @HttpCode(200)
  @Get('/filter')
  getPeopleByFilter(
    @QueryParam('value') value: string,
    @QueryParam('type') type: string
  ) {
    if (!filtersAvailable.includes(type)) {
      throw new BadRequestError('Invalid type of filter');
    }

    const people = peopleProcessing.getByFilter({ value, type });

    return {
      data: people.slice(0, 20),
    };
  }

  @HttpCode(200)
  @Get('/:id')
  getPerson(@Param('id') id: number) {
    const person = peopleProcessing.getById(id);

    if (!person) {
      throw new NotFoundError('No person found');
    }

    return {
      data: person,
    };
  }
}
