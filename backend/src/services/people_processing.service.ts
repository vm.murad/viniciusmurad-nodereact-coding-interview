import people_data from '../data/people_data.json';

export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getAll() {
    return people_data;
  }

  getByFilter({ type, value }: { type: string; value: string }) {
    return people_data.filter(
      (p: any) => p[type] && p[type].toLowerCase() === value.toLowerCase()
    );
  }
}
